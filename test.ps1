$uri = 'https://raw.githubusercontent.com/fgclue/etcher/master/desktop.ini'
$theHashIHave = Get-FileHash desktop.ini -Algorithm SHA256

try {
    $content = Invoke-RestMethod $uri
    $memstream = [System.IO.MemoryStream]::new(
        [System.Text.Encoding]::UTF8.GetBytes($content)
    )
    $thisFileHash = Get-FileHash -InputStream $memstream -Algorithm SHA256
    if($theHashIhave.Hash -eq $thisFileHash.Hash) {
        "all good"
    }
    else {
        "should update here"
    }
}
finally {
    $memstream.foreach('Dispose')
}